package com.dyj.applet.handler;

import com.dyj.applet.domain.query.SupplierSubmitTaskQuery;
import com.dyj.applet.domain.query.SupplierSyncQuery;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DyResult;

/**
 * @author danmo
 * @date 2024-04-28 16:27
 **/
public class LifeServiceHandler extends AbstractAppletHandler {
    public LifeServiceHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 商铺同步
     *
     * @param query 入参
     * @return DyResult<SupplierSyncVo>
     */
    public DyResult<SupplierSyncVo> supplierSync(SupplierSyncQuery query) {
        baseQuery(query);
        return getLifeServicesClient().supplierSync(query);
    }

    /**
     * 查询店铺
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierVo>
     */
    public DyResult<SupplierVo> querySupplier(String supplierExtId) {
        return getLifeServicesClient().querySupplier(baseQuery(), supplierExtId);
    }

    /**
     * 获取抖音POI ID
     *
     * @param amapId 高德POI ID
     * @return DyResult<PoiIdVo>
     */
    public DyResult<PoiIdVo> queryPoiId(String amapId) {
        return getLifeServicesClient().queryPoiId(baseQuery(), amapId);
    }


    /**
     * 店铺匹配任务结果查询
     *
     * @param supplierTaskIds 店铺任务ID
     * @return DyResult<SupplierTaskResultVo>
     */
    public DyResult<SupplierTaskResultVo> querySupplierTaskResult(String supplierTaskIds) {
        return getLifeServicesClient().querySupplierTaskResult(baseQuery(), supplierTaskIds);
    }

    /**
     * 店铺匹配任务状态查询
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierTaskStatusVo>
     */
    public DyResult<SupplierTaskStatusVo> querySupplierMatchStatus(String supplierExtId) {
        return getLifeServicesClient().querySupplierMatchStatus(baseQuery(), supplierExtId);
    }

    /**
     * 提交门店匹配任务
     *
     * @param query 入参
     * @return DyResult<SupplierSubmitTaskVo>
     */
    public DyResult<SupplierSubmitTaskVo> submitSupplierMatchTask(SupplierSubmitTaskQuery query) {
        baseQuery(query);
        return getLifeServicesClient().submitSupplierMatchTask(query);
    }


    /**
     * 查询全部店铺信息接口(天级别请求5次)
     *
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> queryAllSupplier() {
        return getLifeServicesClient().queryAllSupplier(baseQuery());
    }

    /**
     * 查询店铺全部信息任务返回内容
     *
     * @param taskId 任务ID
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> querySupplierCallback(String taskId) {
        return getLifeServicesClient().querySupplierCallback(baseQuery(), taskId);
    }
}
