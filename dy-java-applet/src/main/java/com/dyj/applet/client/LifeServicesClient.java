package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.SupplierSubmitTaskQuery;
import com.dyj.applet.domain.query.SupplierSyncQuery;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 生活服务
 *
 * @author danmo
 * @date 2024-04-28 11:16
 **/
@BaseRequest(baseURL = "${domain}")
public interface LifeServicesClient {

    /**
     * 商铺同步
     *
     * @param query 入参
     * @return DyResult<SupplierSyncVo>
     */
    @Post(url = "supplierSync", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierSyncVo> supplierSync(@JSONBody SupplierSyncQuery query);

    /**
     * 查询店铺
     *
     * @param query         应用信息
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierVo>
     */
    @Get(url = "querySupplier", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierVo> querySupplier(@Var("query") BaseQuery query, @Query("supplier_ext_id") String supplierExtId);

    /**
     * 获取抖音POI ID
     *
     * @param query  应用信息
     * @param amapId 高德POI ID
     * @return DyResult<PoiIdVo>
     */
    @Get(url = "queryPoiId", interceptor = ClientTokenInterceptor.class)
    DyResult<PoiIdVo> queryPoiId(@Var("query") BaseQuery query, @Query("amap_id") String amapId);

    /**
     * 店铺匹配任务结果查询
     *
     * @param query           应用信息
     * @param supplierTaskIds 店铺任务ID
     * @return DyResult<SupplierTaskResultVo>
     */
    @Get(url = "querySupplierTaskResult", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskResultVo> querySupplierTaskResult(@Var("query") BaseQuery query, @Query("supplier_task_ids") String supplierTaskIds);

    /**
     * 店铺匹配任务状态查询
     *
     * @param query         应用信息
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierTaskStatusVo>
     */
    @Get(url = "querySupplierMatchStatus", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskStatusVo> querySupplierMatchStatus(@Var("query") BaseQuery query, @Query("supplier_ext_id") String supplierExtId);

    /**
     * 提交门店匹配任务
     *
     * @param query 入参
     * @return DyResult<SupplierSubmitTaskVo>
     */
    @Post(url = "submitSupplierMatchTask", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierSubmitTaskVo> submitSupplierMatchTask(@JSONBody SupplierSubmitTaskQuery query);

    /**
     * 查询全部店铺信息接口(天级别请求5次)
     * @param query 应用信息
     * @return   DyResult<SupplierTaskVo>
     */
    @Get(url = "queryAllSupplier", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskVo> queryAllSupplier(@Var("query") BaseQuery query);

    /**
     * 查询店铺全部信息任务返回内容
     *
     * @param query  应用信息
     * @param taskId 任务ID
     * @return DyResult<SupplierTaskVo>
     */
    @Get(url = "querySupplierCallback", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskVo> querySupplierCallback(@Var("query") BaseQuery query, @Query("task_id") String taskId);
}
