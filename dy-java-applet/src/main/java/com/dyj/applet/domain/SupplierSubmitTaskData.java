package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 16:16
 **/
public class SupplierSubmitTaskData {

    /**
     * 三方系统内的唯一 id
     */
    private String supplier_ext_id;
    /**
     * 抖音门店 id
     * （若已获取抖音门店 id，可直接用于关联匹配，以提高匹配成功率）
     */
    private String poi_id;
    /**
     * 门店名称
     */
    private String poi_name;
    /**
     * 省份名称
     */
    private String province;
    /**
     * 城市名称
     * （可为空字符串，仅供问题排查使用）
     */
    private String city;
    /**
     * 门店地址信息
     */
    private String address;
    /**
     * 纬度，精度为小数点后6位，需使用 GCJ-02 坐标系统
     */
    private Float latitude;
    /**
     * 经度，精度为小数点后6位，需使用 GCJ-02 坐标系统
     */
    private Float longitude;
    /**
     * 其他信息，可为空字符串
     */
    private String extra;
    /**
     * 营业手机号
     */
    private String contact_phone;
    /**
     * 营业座机号
     */
    private String contact_tel;

    public static SupplierSubmitTaskDataBuilder builder() {
        return new SupplierSubmitTaskDataBuilder();
    }

    public static class SupplierSubmitTaskDataBuilder {
        private String supplierExtId;
        private String poiId;
        private String poiName;
        private String province;
        private String city;
        private String address;
        private Float latitude;
        private Float longitude;
        private String extra;
        private String contactPhone;
        private String contactTel;

        public SupplierSubmitTaskDataBuilder supplierExtId(String supplierExtId) {
            this.supplierExtId = supplierExtId;
            return this;
        }
        public SupplierSubmitTaskDataBuilder poiId(String poiId) {
            this.poiId = poiId;
            return this;
        }
        public SupplierSubmitTaskDataBuilder poiName(String poiName) {
            this.poiName = poiName;
            return this;
        }
        public SupplierSubmitTaskDataBuilder province(String province) {
            this.province = province;
            return this;
        }
        public SupplierSubmitTaskDataBuilder city(String city) {
            this.city = city;
            return this;
        }
        public SupplierSubmitTaskDataBuilder address(String address) {
            this.address = address;
            return this;
        }
        public SupplierSubmitTaskDataBuilder latitude(Float latitude) {
            this.latitude = latitude;
            return this;
        }
        public SupplierSubmitTaskDataBuilder longitude(Float longitude) {
            this.longitude = longitude;
            return this;
        }
        public SupplierSubmitTaskDataBuilder extra(String extra) {
            this.extra = extra;
            return this;
        }
        public SupplierSubmitTaskDataBuilder contactPhone(String contactPhone) {
            this.contactPhone = contactPhone;
            return this;
        }
        public SupplierSubmitTaskDataBuilder contactTel(String contactTel) {
            this.contactTel = contactTel;
            return this;
        }
        public SupplierSubmitTaskData build() {
            SupplierSubmitTaskData supplierSubmitTaskData = new SupplierSubmitTaskData();
            supplierSubmitTaskData.setSupplier_ext_id(supplierExtId);
            supplierSubmitTaskData.setPoi_id(poiId);
            supplierSubmitTaskData.setPoi_name(poiName);
            supplierSubmitTaskData.setProvince(province);
            supplierSubmitTaskData.setCity(city);
            supplierSubmitTaskData.setAddress(address);
            supplierSubmitTaskData.setLatitude(latitude);
            supplierSubmitTaskData.setLongitude(longitude);
            supplierSubmitTaskData.setExtra(extra);
            supplierSubmitTaskData.setContact_phone(contactPhone);
            supplierSubmitTaskData.setContact_tel(contactTel);
            return supplierSubmitTaskData;
        }
    }

    public String getSupplier_ext_id() {
        return supplier_ext_id;
    }

    public void setSupplier_ext_id(String supplier_ext_id) {
        this.supplier_ext_id = supplier_ext_id;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public String getContact_tel() {
        return contact_tel;
    }

    public void setContact_tel(String contact_tel) {
        this.contact_tel = contact_tel;
    }
}
