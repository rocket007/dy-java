package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-04-28 10:54
 **/
public class OrderCizStatusVo {

    /**
     * 定制状态：0-已定制完成 1-未定制完成 2-未知
     */
    private Integer success_code;

    public Integer getSuccess_code() {
        return success_code;
    }

    public void setSuccess_code(Integer success_code) {
        this.success_code = success_code;
    }
}
